use database_layer::*;
use chrono::{TimeZone, Utc};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    create_branches().await?;
    create_employees().await?;
    create_attendances().await?;

    list_branches().await?;
    branch_update().await?;

    employee_listing().await?;
    employee_update().await?;
    employee_listing().await?;

    Ok(())
}

async fn employee_listing() -> anyhow::Result<()> {
    // Lists only employees with ID from 5 to 8 inclusively sorted by their last name
    list_employees(Some(Format::Simple), Some(SortingOrder::Ascending), None, None, Some(5), Some(8)).await?;

    // Lists all employees sorted by branches and sorted by their last name
    list_employees(Some(Format::ByBranches), None, None, None, None, None).await?;

    // Lists all employees with "o" in their first name with all of their attendances and sorted by their last name in descending order
    list_employees(Some(Format::Complete), Some(SortingOrder::Descending), Some("o"), None, None, None).await?;
    Ok(())
}

async fn branch_update() -> anyhow::Result<()> {
    create_branch("TEST", 0, "Test").await?;

    // Only updates the budget
    update_branch("TEST", Some(419), None).await?;
    // Updates budget and description
    update_branch("TEST", Some(420), Some("You will be terminated")).await?;

    close_branch("TEST", "PR").await?;

    Ok(())
}

async fn employee_update() -> anyhow::Result<()> {
    // Someone made too many Does
    update_employee(1, None, Some("Definitely not Doe"), None, None, None, None, None, None).await?;

    // Adele decided to step up her game and moved from HR to MANAGEMENT
    update_employee(9, None, None, None, None, None, None, None, Some("MAN")).await?;

    // Meanwhile Sarah moved from Pear to an Eggplant
    update_employee(11, None, None, None, Some("Eggplant"), Some(42), None, None, None).await?;

    Ok(())
}

async fn create_branches() -> anyhow::Result<()> {
    create_branch("HR", 160000, "Human resources").await?;
    create_branch("DEV", 100, "Development").await?;
    create_branch("PR", 200000, "Advertising").await?;
    create_branch("MAN", 1000000, "Management").await?;
    create_branch("ENTERTAINMENT", 420, "Jesters and stuff").await?;

    // Branch named "HR" already exists
    assert!(create_branch("HR", 0, "fails").await.is_err());
    Ok(())
}

async fn create_employees() -> anyhow::Result<()> {
    create_employee("Jon", "Doe", "jon@doe.com", "Botanical", 638, "Brno", 69420, "HR").await?;
    create_employee("Josh", "Doe", "questionmark@doe.com", "Lemon", 123, "Brno", 63000, "HR").await?;
    create_employee("Joe", "Doe", "scum@xy.com", "Orange", 164, "Brno", 63788, "DEV").await?;
    create_employee("Jacob", "Slim", "george@zx.com", "Watermelon", 102, "Brno", 69420, "DEV").await?;
    create_employee("Bob", "Rich", "bob@df.com", "Mango", 1239, "Brno", 52001, "DEV").await?;
    create_employee("Michael", "Poor", "anonymous@what.com", "Pineapple", 127, "Brno", 59800, "MAN").await?;
    create_employee("Tom", "Brown", "nameless@run.com", "Avocado", 15, "Brno", 69420, "PR").await?;
    create_employee("Amy", "Yellow", "badgirl@out.com", "Carrot", 789, "Praha", 12300, "HR").await?;
    create_employee("Adele", "White", "girl@of.com", "Potato", 12, "Praha", 12300, "PR").await?;
    create_employee("Barbara", "Black", "badboy@ideas.com", "Pear", 123, "Praha", 12300, "MAN").await?;
    create_employee("Sarah", "Blind", "boy@intellij.com", "Apple", 456, "Brno", 62400, "ENTERTAINMENT").await?;

    // Branch named "MADE-UP" doesn't exist
    assert!(create_employee("Jon", "Doe", "jon@doe.com", "Botanical", 638, "Brno", 69420, "MADE-UP").await.is_err());
    Ok(())
}

async fn create_attendances() -> anyhow::Result<()> {
    let today = Utc.with_ymd_and_hms(2022, 12, 1, 0, 0, 0).unwrap();

    // Jacob Slim worked 3 hours for the DEV branch
    create_attendance(5, "DEV", today, AttendanceType::Working, 15, 18).await?;
    // He then decided to become a jester and entertain his friends for an hour
    create_attendance(5, "ENTERTAINMENT", today, AttendanceType::Working, 18, 19).await?;
    // But after that he got tired and went home for the rest of the shift
    create_attendance(5, "DEV", today, AttendanceType::Off, 19, 22).await?;

    // One of his friends decided to work the whole day because he is eager to get rich
    create_attendance(6, "DEV", today, AttendanceType::Working, 8, 20).await?;

    // Their second friend took a long lunch break
    create_attendance(4, "DEV", today, AttendanceType::Working, 8, 11).await?;
    create_attendance(4, "DEV", today, AttendanceType::Working, 13, 20).await?;

    // Meanwhile their lazy HR-ist was caught sleeping during the shift and will be unpaid because of it
    create_attendance(3, "HR", today, AttendanceType::Unpaid, 12, 16).await?;

    // However, one of his HR friends decided to "help him out" and deleted his attendance completely
    delete_attendance(3, "HR", today, 7).await?;
    Ok(())
}