CREATE TYPE attendance_type AS ENUM ('working', 'sick', 'off', 'unpaid');

CREATE TABLE branch (
    name TEXT PRIMARY KEY,
    budget BIGINT NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE employee (
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    email_address TEXT NOT NULL,
    street TEXT NOT NULL,
    house_number SMALLINT NOT NULL,
    city TEXT NOT NULL,
    zip_code INTEGER NOT NULL,
    branch TEXT NOT NULL REFERENCES branch
);

CREATE TABLE attendance (
    employee_id INTEGER REFERENCES employee,
    branch TEXT REFERENCES branch,
    date TIMESTAMP WITH TIME ZONE,
    sequence_number SERIAL,
    attendance_type attendance_type NOT NULL,
    hour_from SMALLINT NOT NULL,
    hour_to SMALLINT NOT NULL,
    PRIMARY KEY (employee_id, branch, date, sequence_number)
);