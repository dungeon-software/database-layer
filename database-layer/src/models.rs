use crate::AttendanceType;
use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct Employee {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email_address: String,
    pub street: String,
    pub house_number: i16,
    pub city: String,
    pub zip_code: i32,
    pub branch: String,
}
impl Display for Employee {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[{}] Employee: {} {} ({}) working for {} [{} {}, {}, {}]",
            self.id,
            self.first_name,
            self.last_name,
            self.email_address,
            self.branch,
            self.street,
            self.house_number,
            self.city,
            self.zip_code
        )
    }
}

#[derive(Debug, Serialize, Deserialize, sqlx::Decode, sqlx::FromRow)]
pub struct Branch {
    pub name: String,
    pub budget: i64,
    pub description: String,
}

impl Display for Branch {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Branch: {} with budget ${} [{}]",
            self.name, self.budget, self.description
        )
    }
}

#[derive(Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct Attendance {
    pub employee_id: i32,
    pub branch: String,
    #[serde(with = "ts_seconds")]
    pub date: DateTime<Utc>,
    pub sequence_number: i32,
    pub attendance_type: AttendanceType,
    pub hour_from: i16,
    pub hour_to: i16,
}

impl Display for Attendance {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Attendance [{}]: For {} branch on {}: between {}-{} [{}. on that day]",
            self.attendance_type,
            self.branch,
            self.date.format("%Y-%m-%d"),
            self.hour_from,
            self.hour_to,
            self.sequence_number
        )
    }
}
