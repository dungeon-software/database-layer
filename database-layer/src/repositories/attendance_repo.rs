use std::sync::Arc;

use crate::AttendanceType;
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use sqlx::PgPool;

use crate::models::Attendance;

#[async_trait]
pub trait AttendanceRepo {
    async fn create_attendance(
        &self,
        employee_id: i32,
        branch: &str,
        date: DateTime<Utc>,
        attendance_type: AttendanceType,
        hour_from: i16,
        hour_to: i16,
    ) -> anyhow::Result<()>;
    async fn list_attendances(&self, employee_id: i32) -> anyhow::Result<Vec<Attendance>>;
    async fn delete_employee_attendances(&self, id: i32) -> anyhow::Result<()>;
    async fn delete_attendance(
        &self,
        employee_id: i32,
        branch_name: &str,
        date: DateTime<Utc>,
        sequence_number: i32,
    ) -> anyhow::Result<()>;
}

pub struct PostgresAttendanceRepo {
    pg_pool: Arc<PgPool>,
}

impl PostgresAttendanceRepo {
    pub fn new(pg_pool: Arc<PgPool>) -> Self {
        Self { pg_pool }
    }
}

#[async_trait]
impl AttendanceRepo for PostgresAttendanceRepo {
    async fn create_attendance(
        &self,
        employee_id: i32,
        branch: &str,
        date: DateTime<Utc>,
        attendance_type: AttendanceType,
        hour_from: i16,
        hour_to: i16,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            "INSERT INTO attendance (employee_id, branch, date, attendance_type, hour_from, hour_to)
             VALUES ($1, $2, $3, $4, $5, $6)",
            employee_id,
            branch,
            date,
            attendance_type as AttendanceType,
            hour_from,
            hour_to
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn list_attendances(&self, employee_id: i32) -> anyhow::Result<Vec<Attendance>> {
        let attendances = sqlx::query_as!(
            Attendance,
            r#"SELECT employee_id, branch, date, sequence_number, attendance_type AS "attendance_type: _", hour_from, hour_to
            FROM attendance
            WHERE employee_id = ($1)
            ORDER BY date DESC"#,
            employee_id
        )
        .fetch_all(&*self.pg_pool)
        .await?;

        Ok(attendances)
    }

    async fn delete_employee_attendances(&self, id: i32) -> anyhow::Result<()> {
        sqlx::query!(
            r#"DELETE FROM attendance
            WHERE employee_id = ($1)"#,
            id
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn delete_attendance(
        &self,
        employee_id: i32,
        branch: &str,
        date: DateTime<Utc>,
        sequence_number: i32,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            "DELETE FROM attendance
            WHERE employee_id = ($1)
            AND branch = ($2)
            AND date = ($3)
            AND sequence_number = ($4)",
            employee_id,
            branch,
            date,
            sequence_number
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }
}
