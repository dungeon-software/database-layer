use std::sync::Arc;

use crate::{Format, SortingOrder};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use sqlx::PgPool;

use crate::models::Employee;

#[async_trait]
pub trait EmployeeRepo {
    async fn create_employee(
        &self,
        first_name: &str,
        last_name: &str,
        email_address: &str,
        street: &str,
        house_number: i16,
        city: &str,
        zip_code: i32,
        branch_name: &str,
    ) -> anyhow::Result<()>;
    async fn update_employee(
        &self,
        id: i32,
        first_name: Option<&str>,
        last_name: Option<&str>,
        email_address: Option<&str>,
        street: Option<&str>,
        house_number: Option<i16>,
        city: Option<&str>,
        zip_code: Option<i32>,
        branch_name: Option<&str>,
    ) -> anyhow::Result<()>;
    async fn delete_employee(&self, id: i32) -> anyhow::Result<()>;
    async fn list_employees(
        &self,
        format: Format,
        sorting: SortingOrder,
        name: Option<&str>,
        last_name: Option<&str>,
        from_id: Option<i32>,
        to_id: Option<i32>,
    ) -> anyhow::Result<Vec<Employee>>;
    async fn timed_branch_transfer(
        &self,
        id: i32,
        to_branch: &str,
        time_of_transfer: DateTime<Utc>,
    ) -> anyhow::Result<()>;
    async fn global_branch_transfer(
        &self,
        from_branch: &str,
        to_branch: &str,
    ) -> anyhow::Result<()>;
}

pub struct PostgresEmployeeRepo {
    pg_pool: Arc<PgPool>,
}

impl PostgresEmployeeRepo {
    pub fn new(pg_pool: Arc<PgPool>) -> Self {
        Self { pg_pool }
    }
}

#[async_trait]
impl EmployeeRepo for PostgresEmployeeRepo {
    async fn create_employee(
        &self,
        first_name: &str,
        last_name: &str,
        email_address: &str,
        street: &str,
        house_number: i16,
        city: &str,
        zip_code: i32,
        branch_name: &str,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            "INSERT INTO employee (first_name, last_name, email_address, street, house_number, city, zip_code, branch)
            VALUES  ($1, $2, $3, $4, $5, $6, $7, $8)",
            first_name, last_name, email_address, street, house_number, city, zip_code, branch_name
        )
            .execute(&*self.pg_pool)
            .await?;

        Ok(())
    }

    async fn update_employee(
        &self,
        id: i32,
        first_name: Option<&str>,
        last_name: Option<&str>,
        email_address: Option<&str>,
        street: Option<&str>,
        house_number: Option<i16>,
        city: Option<&str>,
        zip_code: Option<i32>,
        branch_name: Option<&str>,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            r#"UPDATE employee
            SET first_name = COALESCE($1, first_name),
                last_name = COALESCE($2, last_name),
                email_address = COALESCE($3, email_address),
                street = COALESCE($4, street),
                house_number = COALESCE($5, house_number),
                city = COALESCE($6, city),
                zip_code = COALESCE($7, zip_code),
                branch = COALESCE($8, branch)
            WHERE id = ($9)"#,
            first_name,
            last_name,
            email_address,
            street,
            house_number,
            city,
            zip_code,
            branch_name,
            id
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn delete_employee(&self, id: i32) -> anyhow::Result<()> {
        sqlx::query!(
            r#"DELETE FROM employee
            WHERE id = ($1)"#,
            id
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn list_employees(
        &self,
        format: Format,
        sorting: SortingOrder,
        first_name: Option<&str>,
        last_name: Option<&str>,
        from_id: Option<i32>,
        to_id: Option<i32>,
    ) -> anyhow::Result<Vec<Employee>> {
        let id_range = match (from_id, to_id) {
            (None, None) => (i32::MIN, i32::MAX),
            (Some(x), None) => (x, i32::MAX),
            (None, Some(x)) => (i32::MIN, x),
            (Some(x), Some(y)) => (x, y),
        };

        let mut format = match format {
            Format::ByBranches => "branch, last_name".to_string(),
            Format::Simple | Format::Complete => "last_name".to_string(),
        };

        match sorting {
            SortingOrder::Ascending => format.push_str(" ASC"),
            SortingOrder::Descending => format.push_str(" DESC"),
        };

        let employees = sqlx::query_as!(
            Employee,
            "SELECT * FROM employee
            WHERE first_name LIKE $1 AND last_name LIKE $2 AND id BETWEEN $3 AND $4
            ORDER BY $5",
            match first_name {
                Some(fragment) => format!("%{}%", fragment),
                None => String::from("%"),
            },
            match last_name {
                Some(fragment) => format!("%{}%", fragment),
                None => String::from("%"),
            },
            id_range.0,
            id_range.1,
            format
        )
        .fetch_all(&*self.pg_pool)
        .await?;

        Ok(employees)
    }

    async fn timed_branch_transfer(
        &self,
        _id: i32,
        _to_branch: &str,
        _time_of_transfer: DateTime<Utc>,
    ) -> anyhow::Result<()> {
        todo!()
    }

    async fn global_branch_transfer(
        &self,
        from_branch: &str,
        to_branch: &str,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            r#"UPDATE employee
            SET branch = ($1)
            WHERE branch = ($2)"#,
            to_branch,
            from_branch
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }
}
