use std::sync::Arc;

use async_trait::async_trait;
use sqlx::PgPool;

use crate::models::Branch;

#[async_trait]
pub trait BranchRepo {
    async fn create_branch(&self, name: &str, budget: i64, description: &str)
        -> anyhow::Result<()>;
    async fn list_branches(&self) -> anyhow::Result<()>;
    async fn update_branch(
        &self,
        branch_name: &str,
        budget: Option<i64>,
        description: Option<&str>,
    ) -> anyhow::Result<()>;
    async fn delete_branch(&self, branch_nane: &str) -> anyhow::Result<()>;
}

pub struct PostgresBranchRepo {
    pg_pool: Arc<PgPool>,
}

impl PostgresBranchRepo {
    pub fn new(pg_pool: Arc<PgPool>) -> Self {
        Self { pg_pool }
    }
}

#[async_trait]
impl BranchRepo for PostgresBranchRepo {
    async fn create_branch(
        &self,
        name: &str,
        budget: i64,
        description: &str,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            "INSERT INTO branch (name, budget, description)
             VALUES ($1, $2, $3)",
            name,
            budget,
            description
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn list_branches(&self) -> anyhow::Result<()> {
        let branches = sqlx::query_as!(Branch, "SELECT * FROM branch ORDER BY name")
            .fetch_all(&*self.pg_pool)
            .await?;

        println!("\n--- BRANCHES ---");
        for (index, branch) in branches.into_iter().enumerate() {
            println!("{}. {}", index + 1, branch)
        }

        Ok(())
    }

    async fn update_branch(
        &self,
        branch_name: &str,
        budget: Option<i64>,
        description: Option<&str>,
    ) -> anyhow::Result<()> {
        sqlx::query!(
            "UPDATE branch
            SET budget = COALESCE($1, budget),
                description = COALESCE($2, description)
            WHERE name = ($3)",
            budget,
            description,
            branch_name
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }

    async fn delete_branch(&self, branch_to_delete: &str) -> anyhow::Result<()> {
        sqlx::query!(
            "DELETE FROM branch
            WHERE name = ($1)",
            branch_to_delete
        )
        .execute(&*self.pg_pool)
        .await?;

        Ok(())
    }
}
