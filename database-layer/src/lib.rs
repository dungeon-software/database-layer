use std::env;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

use async_once::AsyncOnce;
use chrono::{DateTime, Utc};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};

use repositories::attendance_repo::{AttendanceRepo, PostgresAttendanceRepo};
use repositories::branch_repo::{BranchRepo, PostgresBranchRepo};
use repositories::employee_repo::{EmployeeRepo, PostgresEmployeeRepo};

use crate::models::Employee;

mod models;
mod repositories;

/// Complete format lists employees ordered by their last name with their complete attendance records
/// ByBranches format sorts and lists employees by branches and their last name
/// Simple format just lists all of the employees sorted by their last name
#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Format {
    Complete,
    ByBranches,
    Simple,
}

/// Sorting order of output records
#[derive(Eq, PartialEq)]
pub enum SortingOrder {
    Ascending,
    Descending,
}

#[derive(Debug, Serialize, Deserialize, sqlx::Type)]
#[sqlx(type_name = "attendance_type", rename_all = "lowercase")]
pub enum AttendanceType {
    Working,
    Sick,
    Off,
    Unpaid,
}

impl Display for AttendanceType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = match self {
            AttendanceType::Working => "Working",
            AttendanceType::Sick => "Sick",
            AttendanceType::Off => "Off",
            AttendanceType::Unpaid => "Unpaid",
        };

        f.write_str(string)
    }
}

lazy_static! {
    pub static ref LIBRARY: AsyncOnce<Library> = AsyncOnce::new(async { Library::new().await });
}

pub struct Library {
    attendances: PostgresAttendanceRepo,
    branches: PostgresBranchRepo,
    employees: PostgresEmployeeRepo,
}

impl Library {
    pub async fn new() -> Self {
        let pool = Arc::new(setup_pool().await.expect("Failed initializing connection"));
        Self {
            attendances: PostgresAttendanceRepo::new(pool.clone()),
            branches: PostgresBranchRepo::new(pool.clone()),
            employees: PostgresEmployeeRepo::new(pool.clone()),
        }
    }
}

async fn setup_pool() -> anyhow::Result<Pool<Postgres>> {
    dotenvy::dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let db_pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;

    sqlx::migrate!().run(&db_pool).await?;

    Ok(db_pool)
}

#[doc = "Creates a new branch"]
pub async fn create_branch(name: &str, budget: i64, description: &str) -> anyhow::Result<()> {
    LIBRARY
        .get()
        .await
        .branches
        .create_branch(name, budget, description)
        .await?;

    Ok(())
}

#[doc = "Lists all branches"]
pub async fn list_branches() -> anyhow::Result<()> {
    LIBRARY.get().await.branches.list_branches().await?;

    Ok(())
}

#[doc = "Updates a branch with the given name. \
Arguments can be optional, in that case, the value in the database will stay unaffected."]
pub async fn update_branch(
    branch_name: &str,
    budget: Option<i64>,
    description: Option<&str>,
) -> anyhow::Result<()> {
    if budget.is_none() && description.is_none() {
        return Ok(());
    }

    LIBRARY
        .get()
        .await
        .branches
        .update_branch(branch_name, budget, description)
        .await?;

    Ok(())
}

#[doc = "Closes the branch and moves all of its employees to the transfer branch.\
All attendance records will stay intact."]
pub async fn close_branch(branch_to_delete: &str, transition_branch: &str) -> anyhow::Result<()> {
    LIBRARY
        .get()
        .await
        .employees
        .global_branch_transfer(branch_to_delete, transition_branch)
        .await?;

    Ok(())
}

#[doc = "Creates a new employee record with given values"]
pub async fn create_employee(
    first_name: &str,
    last_name: &str,
    email_address: &str,
    street: &str,
    house_number: i16,
    city: &str,
    zip_code: i32,
    branch_name: &str,
) -> anyhow::Result<()> {
    LIBRARY
        .get()
        .await
        .employees
        .create_employee(
            first_name,
            last_name,
            email_address,
            street,
            house_number,
            city,
            zip_code,
            branch_name,
        )
        .await?;

    Ok(())
}

async fn complete_employee_list(employees: Vec<Employee>) -> anyhow::Result<()> {
    let library = LIBRARY.get().await;

    println!("\n--- EMPLOYEES - COMPLETE FORMAT ---");
    for employee in employees {
        println!("{}", employee);
        let attendances = library.attendances.list_attendances(employee.id).await?;
        for attendance in attendances {
            println!("    {}", attendance);
        }
    }

    Ok(())
}

async fn by_branches_employee_list(employees: Vec<Employee>) -> anyhow::Result<()> {
    let mut current_branch = String::new();

    println!("\n--- EMPLOYEES - BY BRANCHES FORMAT ---");
    for employee in employees {
        if employee.branch != current_branch {
            current_branch = employee.branch.clone();
            println!("[{}]", current_branch);
        }
        println!("    {}", employee);
    }

    Ok(())
}

async fn simple_employee_list(employees: Vec<Employee>) -> anyhow::Result<()> {
    println!("\n--- EMPLOYEES - SIMPLE FORMAT ---");
    for employee in employees {
        println!("{}", employee);
    }

    Ok(())
}

#[doc = "Lists all employees by given format and sorting order filtered using given filters. \
Format specifies the visual output format.\
Sorting order specifies which way the data is sorted.\
Additionally, user can filter records using their names and IDs.\
Name filters match substrings in record's names while ID filters specify a range in which the records belong.\
If no format or sorting order is given, the function will work as Simple output format with ascending sorting."]
pub async fn list_employees(
    format: Option<Format>,
    sorting_order: Option<SortingOrder>,
    first_name: Option<&str>,
    last_name: Option<&str>,
    from_id: Option<i32>,
    to_id: Option<i32>,
) -> anyhow::Result<()> {
    let format = match format {
        Some(format) => format,
        None => Format::Simple,
    };

    let sorting_order = match sorting_order {
        Some(order) => order,
        None => SortingOrder::Ascending,
    };

    let employees = LIBRARY
        .get()
        .await
        .employees
        .list_employees(format, sorting_order, first_name, last_name, from_id, to_id)
        .await?;

    match format {
        Format::Complete => complete_employee_list(employees).await?,
        Format::ByBranches => by_branches_employee_list(employees).await?,
        Format::Simple => simple_employee_list(employees).await?,
    }

    Ok(())
}

#[doc = "Updates an employee with the given ID. \
Arguments can be optional, in that case, the value in the database will stay unaffected."]
pub async fn update_employee(
    id: i32,
    first_name: Option<&str>,
    last_name: Option<&str>,
    email_address: Option<&str>,
    street: Option<&str>,
    house_number: Option<i16>,
    city: Option<&str>,
    zip_code: Option<i32>,
    branch_name: Option<&str>,
) -> anyhow::Result<()> {
    if first_name.is_none()
        && last_name.is_none()
        && email_address.is_none()
        && street.is_none()
        && house_number.is_none()
        && city.is_none()
        && zip_code.is_none()
        && branch_name.is_none()
    {
        return Ok(());
    }

    LIBRARY
        .get()
        .await
        .employees
        .update_employee(
            id,
            first_name,
            last_name,
            email_address,
            street,
            house_number,
            city,
            zip_code,
            branch_name,
        )
        .await?;

    Ok(())
}

#[doc = "Deletes an employee with the given ID from the DB"]
pub async fn delete_employee(id: i32) -> anyhow::Result<()> {
    let library = LIBRARY.get().await;

    library.attendances.delete_employee_attendances(id).await?;
    library.employees.delete_employee(id).await?;

    Ok(())
}

#[doc = "Creates a new employee attendance"]
pub async fn create_attendance(
    employee_id: i32,
    branch: &str,
    date: DateTime<Utc>,
    attendance_type: AttendanceType,
    hour_from: i16,
    hour_to: i16,
) -> anyhow::Result<()> {
    LIBRARY
        .get()
        .await
        .attendances
        .create_attendance(
            employee_id,
            branch,
            date,
            attendance_type,
            hour_from,
            hour_to,
        )
        .await?;

    Ok(())
}

#[doc = "Deletes an employee attendance"]
pub async fn delete_attendance(
    employee_id: i32,
    branch: &str,
    date: DateTime<Utc>,
    sequence_number: i32,
) -> anyhow::Result<()> {
    LIBRARY
        .get()
        .await
        .attendances
        .delete_attendance(employee_id, branch, date, sequence_number)
        .await?;

    Ok(())
}
